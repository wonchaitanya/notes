# Links

## Language Processing
+ [Stanford NLP with Deep Learning](https://web.stanford.edu/class/archive/cs/cs224n/cs224n.1184/syllabus.html)
+ [Global Vectors for Word Representation](https://nlp.stanford.edu/projects/glove/)

## Programming
+ [JavaAid Youtube](https://www.youtube.com/channel/UCx1hbK753l3WhwXP5r93eYA/playlists?disable_polymer=1)
+ [Python Linear Algebra](https://docs.scipy.org/doc/numpy-1.15.0/reference/routines.linalg.html#matrix-and-vector-products)
+ [functional javascript part1: lists](https://blog.jeremyfairbank.com/javascript/functional-javascript-lists-1/)
+ [Make Tutorial](http://mrbook.org/blog/tutorials/make)

## DevOps
+ [pulumi](https://www.pulumi.com/)
+ [setting up continuous integration continuous deployment with jenkins](https://code.tutsplus.com/tutorials/setting-up-continuous-integration-continuous-deployment-with-jenkins--cms-21511)
+ [zeit now](https://zeit.co/now)

## Electric Cars
+ [teknomadix](https://teknomadix.com/)

## Deep Learning
+ [which gpu for deep learning](https://timdettmers.com/2019/04/03/which-gpu-for-deep-learning/)

## Dependency Injection
+ [dependency injection for beginners](https://blog.usejournal.com/dependency-injection-for-beginners-56c643363e92)

## Kubernetes
+ [creating image pull secret for gcr](https://stackoverflow.com/questions/36283660/creating-image-pull-secret-for-google-container-registry-that-doesnt-expire)
+ [kubectl run with imagepullsecrits](https://ekartco.com/2017/10/kubernetes-kubectl-run-with-imagepullsecrets/)
+ [dockerizing a react application](https://medium.com/ai2-blog/dockerizing-a-react-application-3563688a2378)
+ [images](https://kubernetes.io/docs/concepts/containers/images/)
+ [using google container registry](https://ryaneschinger.com/blog/using-google-container-registry-gcr-with-minikube/)
+ [katacoda](https://www.katacoda.com/)
+ [kubernetes external ip](https://stackoverflow.com/questions/44110876/kubernetes-service-external-ip-pending)
+ [kubernetes raspberri pi](https://github.com/kubernetes/kube-deploy/issues/220)
+ [kubernetes bare metal](https://itnext.io/install-kubernetes-on-bare-metal-centos7-fba40e9bb3de)
+ [access pods outside of cluster](http://alesnosek.com/blog/2017/02/14/accessing-kubernetes-pods-from-outside-of-the-cluster/)
+ [connect application to services](https://kubernetes.io/docs/concepts/services-networking/connect-applications-service/#creating-a-service)
+ [gcloud container registry quickstart](https://cloud.google.com/container-registry/docs/quickstart)
+ [gcloud ubuntu](https://cloud.google.com/sdk/docs/quickstart-debian-ubuntu)
+ [install gcloud SDK](https://cloud.google.com/sdk/docs/downloads-apt-get)
+ [private container registry](https://github.com/kubernetes/kubernetes/tree/release-1.9/cluster/addons/registry)
+ [create docker image](https://www.mirantis.com/blog/how-do-i-create-a-new-docker-image-for-my-application/)
+ [kubectl commands](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#-strong-getting-started-strong-)
+ [kubectl deploy app](https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-app/deploy-intro/)

## Web Application
+ [webpack-tutorials](https://www.valentinog.com/blog/webpack-tutorial/)
+ [shopping cart](https://github.com/mrvautin/expressCart)
+ [react tutorial](https://reactjs.org/docs/hello-world.html)
+ [react-webpack-simple](https://github.com/jsphbtst/react-webpack-simple)
+ [simple react todo](https://github.com/aghh1504/simple-react-todo-list#npm-start)
+ [express-js-produces-404-for-js-file-that-clearly-exists](https://stackoverflow.com/questions/27565411/express-js-produces-404-for-js-file-that-clearly-exists)
+ [whats-the-correct-way-to-serve-production-react-bundle-js-built-by-webpack](https://stackoverflow.com/questions/35928766/whats-the-correct-way-to-serve-production-react-bundle-js-built-by-webpack)
+ [simple_webpack_boilerplate](https://github.com/pinglinh/simple_webpack_boilerplate)
+ [part-1-react-app-from-scratch-using-webpack-4](https://medium.freecodecamp.org/part-1-react-app-from-scratch-using-webpack-4-562b1d231e75)
+ [node express mongo](https://github.com/DanWahlin/NodeExpressMongoDBDockerApp)
+ [react docker](https://www.telerik.com/blogs/dockerizing-react-applications-for-continuous-integration)
+ [scotch post parameters](https://scotch.io/tutorials/use-expressjs-to-get-url-and-post-parameters)
+ [mongo node crud](https://codeburst.io/writing-a-crud-app-with-node-js-and-mongodb-e0827cbbdafb)
+ [Hapi Mongo](https://patrick-meier.io/build-a-restful-api-using-hapi-js-and-mongodb/)
+ [Mongo and Docker](https://hub.docker.com/_/mongo/)
+ [yanb](https://github.com/GenFirst/yanb)
+ [Docker Node Mongo](https://github.com/KrunalLathiya/DockerNodeMongo)
+ [MEAN example](https://github.com/GenFirst/yanb)
+ [forms](https://stackoverflow.com/questions/133925/javascript-post-request-like-a-form-submit)
+ [CRUD Express MongoDB](https://zellwk.com/blog/crud-express-mongodb/)
+ [Express, Node and Docker](https://github.com/mpayetta/express-node-docker)

## Misc
+ [Josan Gonzalez](https://www.artstation.com/josan)
+ [Josan Gonzalez Deviant Art](https://www.deviantart.com/f1x-2)

## Parts
+ [Boundary Devices](https://boundarydevices.com/)
+ [kendryte](https://kendryte.com/)
+ [superbrightleds](https://www.superbrightleds.com)
+ [Mobile Defenders](https://www.mobiledefenders.com)
+ [Cellular Parts USA](http://www.cellularpartsusa.com)
+ [Electro-Gems](https://www.electrogems.com/)
+ [Google Developer Web Fundamentals](https://developers.google.com/web/fundamentals/)