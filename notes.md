# Misc Notes

## Setting up a .ssh/config file
+ [document showing ssh permissions](https://gist.github.com/grenade/6318301)
+ `touch ~/.ssh/config && chmod 644 ~/.ssh/config`

### Contents of config
```
Host <arbitrary name>
HostName <ip address>
User <user>
```

## Ubuntu sftp using nautilus
`sftp://<user>@<ip address>`

## Chromebook setup
+ `sudo apt-get install gnupg`
+ install nodejs using current methods


## Digital Ocean Instance Set Up
+ `sudo apt-get install nodejs npm docker-compose`
+ install docker
+ `sudo chown ubuntu:ubuntu /mnt/storage/`

## Node Listen on port 80
+ `sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 3000`


## Mongo
+ `docker exec -it mongoContainer mongo`

## GPG
+ install gnupg `sudo apt-get install gnupg`
+ list public keys `gpg --list-keys`
+ list private keys `gpg --list-secret-keys`
+ import private key `gpg --import ./<private key>`
+ trust key `gpg --edit-key <private key name>`
    + `trust`
## SoundJuicer on Ubuntu
+ `sudo apt-get install sound-juicer`

## Ubuntu connect to android phone via USB
+ use a usb cable capable of transfering files
+ `sudo apt-get install libmtp9`
+ `sudo apt-get install jmtpfs`
+ make a directory: ex. `mkdir temp`
+ mount directory `jmtpfs ./temp`
+ answer yes when prompted on the phone
+ unmount the directory when finished `sudo umount -l ./temp`


## Node
+ [NodeJS](https://nodejs.org/en/)
+ Install Node using NVM

## nvm is not compatible with the npm config "prefix"
+ `npm config delete prefix`
+ `npm config set prefix $NVM_DIR/versions/node/v<version in error>`

### NVM
+ `source $(brew --prefix nvm)/nvm.sh`
+ install, use <ver>
+ Check the latest [version](https://github.com/creationix/nvm/releases)

#### Installation
```
apt-get update
apt-get install build-essential libssl-dev
curl https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
```
