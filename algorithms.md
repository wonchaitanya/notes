# Misc Topics from Discrete Math

## Sum of integers 1 to N

```math
N = \frac{n (n + 1)}{2}
```
## Fibbonacci

+ Every third number is even

```math
E(n)=4E(n-1)-E(n-2)
```

+ Equivilently

```math
F(n)=4F(n-3)+F(n-6)
```

## Integers
+ Slicing - Given an integer N, the rightmost m numbers

```math
M = N \bmod 10^m
```

## Prime Number

### Some useful facts:
+ 1 is not a prime
+ All primes except 2 are odd
+ All primes greater than 3 can be written in the form 6k+/-1.
+ Any number n can have only one primefactor greater than n .
+ If we cannot find a number f less than or equal n that divides n then n is prime

### Sieve of Eratosthenes
An ancient algorithm for finding all the prime numbers less than some number
+ [Sieve of Eratosthenes](sieve-of-eratosthenes.py)


## Factors
### Fast algorithms to find find factors of a number
+ [Find all prime factors](prime-factors.py)
+ [Find all factors](factors.py)

### Finding all factors using primes
Using the fundemental theorem of arithmetic

```math
N = p_1^{q_1} p_2^{q_2} ... p_m^{q_m}
```

Find factors of a number by numerating all combinations of prime factors


+ [Find all factors using primes](factors-from-primes.py)
