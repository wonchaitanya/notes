def findFactors(n):
    r = []
    i = 2
    while i <= math.sqrt(n):
        if n % i == 0:
            r.append(i)
            if i != (n / i):
                r.append(n/i)
        i = i + 1
    return r